package com.ipl;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


    public class Main {
        public static final int ID = 0;
        public static final int SEASON = 1;
        public static final int WINNER = 10;
        public static final int MATCH_ID = 0;
        public static final int BOWLING_TEAM = 3;
        public static final int Batting_Team=2;
        public static final int EXTRA_RUNS = 16;
        public static final int TOTALRUNS = 17;
        public static final int BOWLERS = 8;
        public static final int BATSMAN = 6;
        public static final int BATSMAN_RUN = 15;
        public static final int Player_Of_Match=13;
        public static final int Toss_Winner=6;
        public static final int No_Ball=13;
        public static final int Dismissal_Kind=17;
        public static void main(String[] args){
            List<Match> matches_data = getMatchesData();
            List<Delivery> deliveries_data = getDeliveryData();

            findNumberOfMatchesPlayedPerYear(matches_data);

        }
        public static List<Match> getMatchesData(){
            List<Match> matches = new ArrayList();
            String linematch = "";
            try {
                BufferedReader br = new BufferedReader(new FileReader("./src/data/matches.csv"));
                br.readLine();
                while ((linematch = br.readLine()) != null) {
                    Match match = new Match();
                    String[] matchData = linematch.split(",");
                    match.setId(Integer.parseInt(matchData[ID]));
                    match.setSeason(Integer.parseInt(matchData[SEASON]));
                    match.setWinner(matchData[WINNER]);
                    match.setPlayerOfMatch(matchData[Player_Of_Match]);
                    match.setTossWinner((matchData[Toss_Winner]));
                    matches.add(match);
                }
            }catch (Exception e) {
                e.printStackTrace();
            }
            return matches;

        }
        public static List<Delivery> getDeliveryData(){

            List<Delivery> deliveries = new ArrayList();
            String linematch = "";
            try {
                BufferedReader br = new BufferedReader(new FileReader("./src/data/deliveries.csv"));

                br.readLine();

                while ((linematch = br.readLine()) != null) {
                    Delivery delivery = new Delivery();
                    String[] delivery_Data = linematch.split(",");
                    delivery.setMatchId(Integer.parseInt(delivery_Data[MATCH_ID]));
                    delivery.setBowlingTeam(delivery_Data[BOWLING_TEAM]);
                    delivery.setExtraRuns(Integer.parseInt(delivery_Data[EXTRA_RUNS]));
                    delivery.setBowlerName(delivery_Data[BOWLERS]);
                    delivery.setTotalRuns(Integer.parseInt(delivery_Data[TOTALRUNS]));
                    delivery.setBatsmanName(delivery_Data[BATSMAN]);
                    delivery.setBatsmanRuns(Integer.parseInt(delivery_Data[BATSMAN_RUN]));
                    delivery.setNoBall(Integer.parseInt(delivery_Data[No_Ball]));
                    delivery.setDismissalKind(delivery_Data[Dismissal_Kind]);
                    delivery.setBattingTeam(delivery_Data[Batting_Team]);
                    deliveries.add(delivery);
                }

            }catch (Exception e) {
                e.printStackTrace();
            }
            return deliveries;

        }


        public static void findNumberOfMatchesPlayedPerYear(List<Match>matches_data){
            HashMap<Integer, Integer> TotalOfMatchesPlayedPerYear
                    = new HashMap<Integer, Integer>();
            for(int i=0;i<matches_data.size();i++){
                Match match = matches_data.get(i);
                int season = match.getSeason();
                if(TotalOfMatchesPlayedPerYear.containsKey(season)){
                    TotalOfMatchesPlayedPerYear.put(season,TotalOfMatchesPlayedPerYear.get(season) + 1);
                }
                else{
                    TotalOfMatchesPlayedPerYear.put(season,1);
                }
            }
            System.out.println(TotalOfMatchesPlayedPerYear);
        }

    }
